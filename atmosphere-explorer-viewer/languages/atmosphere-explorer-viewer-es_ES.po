msgid ""
msgstr ""
"Project-Id-Version: aeviewer\n"
"POT-Creation-Date: 2015-10-29 19:50-0500\n"
"PO-Revision-Date: 2015-10-29 19:50-0500\n"
"Last-Translator: \n"
"Language-Team: Távara Digital <javier@tavara.pe>\n"
"Language: es_ES\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.6\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-SearchPath-0: .\n"

#: atmosphere-explorer-viewer.php:99
msgid "Select a logger"
msgstr "Selecciona una estación"

#: atmosphere-explorer-viewer.php:151
#, php-format
msgid "Channel #%s"
msgstr "Canal #%s"

#: atmosphere-explorer-viewer.php:154
#, php-format
msgid "Sensor height: %s m."
msgstr "Altura del sensor: %s m."
